package tech.garymyers.wms.extensions

import java.util.Optional

fun <T> Optional<T>.orNull(): T? {
   return if (this.isPresent) {
      this.get()
   } else {
      null
   }
}
