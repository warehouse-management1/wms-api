package tech.garymyers.wms.domain

import com.google.common.base.CaseFormat.LOWER_CAMEL
import com.google.common.base.CaseFormat.LOWER_UNDERSCORE
import com.hightouchtechnologies.commerce.domain.ValidPageSortBy
import io.micronaut.core.annotation.Introspected
import io.micronaut.data.model.Pageable
import io.micronaut.data.model.Sort
import io.swagger.v3.oas.annotations.media.Schema
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import tech.garymyers.wms.domain.ControllerPageRequestDefaults.DEFAULT_PAGE
import tech.garymyers.wms.domain.ControllerPageRequestDefaults.DEFAULT_SIZE
import tech.garymyers.wms.domain.ControllerPageRequestDefaults.DEFAULT_SORT_BY
import tech.garymyers.wms.domain.ControllerPageRequestDefaults.DEFAULT_SORT_DIRECTION
import tech.garymyers.wms.extensions.isAllSameCase
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern
import javax.validation.constraints.Pattern.Flag.CASE_INSENSITIVE

object ControllerPageRequestDefaults {
   const val DEFAULT_PAGE: Int = 1
   const val DEFAULT_SIZE: Int = 10
   const val DEFAULT_SORT_BY: String = "id"
   const val DEFAULT_SORT_DIRECTION: String = "ASC"
}

interface ControllerPageRequest {
   fun page(): Int
   fun size(): Int
   fun sortBy(): String
   fun sortDirection(): String
   fun snakeSortBy(): String
   fun offset(): Int
   fun nextPage(): ControllerPageRequest
   fun copyFillInDefaults(): ControllerPageRequest
   fun toPageable(): Pageable
}

@Schema(
   name = "PageRequestBase",
   title = "Basic implementation of a page request"
)
@Introspected
abstract class ControllerPageRequestBase<out PAGE : ControllerPageRequest>(

   @field:Min(value = 1)
   @field:Schema(minimum = "1", description = "The page that is requested.  Starts with 1", defaultValue = "1")
   var page: Int?,

   @field:Min(value = 5)
   @field:Max(value = 100)
   @field:Schema(minimum = "5", description = "How many items for each page", defaultValue = "10")
   var size: Int?,

   @field:Schema(description = "The column to sort the data by.  Currently only id and name are supported", defaultValue = "id")
   var sortBy: String?,

   @field:Pattern(regexp = "ASC|DESC", flags = [CASE_INSENSITIVE])
   @field:Schema(description = "The direction the results should be sorted by.  Either Ascending or Descending", defaultValue = "ASC")
   var sortDirection: String?

) : ControllerPageRequest {

   protected abstract fun myCopyPage(page: Int, size: Int, sortBy: String, sortDirection: String): PAGE
   protected abstract fun sortByMe(): String
   protected abstract fun myToStringValues(): List<Pair<String, Any?>>

   final override fun page(): Int = page ?: DEFAULT_PAGE
   final override fun size(): Int = size ?: DEFAULT_SIZE
   final override fun sortBy(): String = sortBy ?: DEFAULT_SORT_BY
   final override fun sortDirection(): String = sortDirection ?: DEFAULT_SORT_DIRECTION
   final override fun nextPage(): PAGE = myCopyPage(this.page() + 1, size(), sortBy(), sortDirection())
   final override fun copyFillInDefaults(): PAGE = myCopyPage(this.page(), size(), sortBy(), sortDirection())

   final override fun snakeSortBy(): String {
      val sortByMe = sortByMe()

      return if (sortByMe.isAllSameCase()) {
         sortBy()
      } else {
         LOWER_CAMEL.to(LOWER_UNDERSCORE, sortByMe())
      }
   }

   final override fun offset(): Int {
      val requestedOffsetPage: Int = page()
      val offsetPage = requestedOffsetPage - 1
      val offsetSize = size()

      return offsetPage * offsetSize
   }

   override fun equals(other: Any?): Boolean =
      if (other is ControllerPageRequestBase<*>) {
         EqualsBuilder()
            .append(this.page, other.page)
            .append(this.size, other.size)
            .append(this.sortBy, other.sortBy)
            .append(this.sortDirection, other.sortDirection)
            .isEquals
      } else {
         false
      }

   override fun hashCode(): Int =
      HashCodeBuilder()
         .append(this.page)
         .append(this.size)
         .append(this.sortBy)
         .append(this.sortDirection)
         .toHashCode()

   final override fun toString(): String { // = myToString("?page=$page&size=$size&sortBy=$sortBy&sortDirection=$sortDirection")
      val stringBuilder = StringBuilder()
      var separator = "?"

      separator = page?.apply { stringBuilder.append(separator).append("page=").append(this) }?.let { "&" } ?: separator
      separator = size?.apply { stringBuilder.append(separator).append("size=").append(this) }?.let { "&" } ?: separator
      separator = sortBy?.apply { stringBuilder.append(separator).append("sortBy=").append(this) }?.let { "&" } ?: separator
      separator = sortDirection?.apply { stringBuilder.append(separator).append("sortDirection=").append(this) }?.let { "&" } ?: separator

      myToStringValues().asSequence()
         .filter { (_, value) -> value != null }
         .flatMap { (key, value) ->
            if (value is Iterable<*>) {
               value.asSequence().map { key to it }
            } else {
               sequenceOf(key to value)
            }
         }
         .forEach { (key, value) ->
            separator = value.apply { stringBuilder.append(separator).append(key).append('=').append(this) }.let { "&" }
         }

      return stringBuilder.toString()
   }
}

@Schema(
   name = "StandardPageRequest",
   title = "How to query for a paged set of items",
   description = "This is the form of the URL parameters that can be used to query for a subset of a larger dataset. Example: ?page=1&size=10&sortBy=id&sortDirection=ASC",
   allOf = [ControllerPageRequestBase::class]
)
@Introspected
class StandardControllerPageRequest(
   page: Int? = null,
   size: Int? = null,
   sortBy: String? = null,
   sortDirection: String? = null
) : ControllerPageRequestBase<StandardControllerPageRequest>(page, size, sortBy, sortDirection) {

   constructor(pageRequestIn: ControllerPageRequest? = null) :
      this(
         page = pageRequestIn?.page() ?: DEFAULT_PAGE,
         size = pageRequestIn?.size() ?: DEFAULT_SIZE,
         sortBy = pageRequestIn?.sortBy() ?: DEFAULT_SORT_BY,
         sortDirection = pageRequestIn?.sortDirection() ?: DEFAULT_SORT_DIRECTION
      )

   override fun myCopyPage(page: Int, size: Int, sortBy: String, sortDirection: String): StandardControllerPageRequest =
      StandardControllerPageRequest(
         page = page,
         size = size,
         sortBy = sortBy,
         sortDirection = sortDirection
      )

   @ValidPageSortBy("id")
   override fun sortByMe(): String = sortBy()
   override fun myToStringValues(): List<Pair<String, Any?>> = emptyList()
   override fun toPageable() =
      Pageable.from(
         this.page() - 1,
         this.size(),
         Sort.of(Sort.Order(sortByMe(), Sort.Order.Direction.valueOf(sortDirection()), false))
      )
}
