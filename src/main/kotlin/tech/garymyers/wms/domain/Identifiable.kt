package tech.garymyers.wms.domain

import java.util.UUID

interface Identifiable {
   fun myId(): UUID?
}
