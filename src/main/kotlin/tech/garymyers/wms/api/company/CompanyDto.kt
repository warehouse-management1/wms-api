package tech.garymyers.wms.api.company

import io.micronaut.core.annotation.Introspected
import java.util.UUID
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Introspected
data class CompanyDto(

   var id: UUID? = null,

   @field:NotBlank
   @field:Size(min = 3, max = 50)
   var name: String? = null,
) {
   constructor(entity: Company) :
      this(
         id = entity.id,
         name = entity.name,
      )
}
