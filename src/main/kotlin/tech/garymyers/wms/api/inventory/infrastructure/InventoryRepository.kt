package tech.garymyers.wms.api.inventory.infrastructure

import io.micronaut.data.annotation.Join
import io.micronaut.data.annotation.repeatable.JoinSpecifications
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.data.repository.PageableRepository
import tech.garymyers.wms.api.inventory.Inventory
import tech.garymyers.wms.api.warehouse.Warehouse
import java.util.Optional
import java.util.UUID

@JdbcRepository
interface InventoryRepository : PageableRepository<Inventory, UUID> {

   fun save(inventory: Inventory): Inventory

   fun update(inventory: Inventory): Inventory

   @JoinSpecifications(
      Join(value = "warehouse"),
      Join(value = "warehouse.company")
   )
   override fun findById(id: UUID): Optional<Inventory>

   @JoinSpecifications(
      Join(value = "warehouse"),
      Join(value = "warehouse.company")
   )
   fun findByWarehouse(warehouse: Warehouse, pageable: Pageable): Page<Inventory>
}
