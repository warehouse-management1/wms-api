package tech.garymyers.wms.api.inventory

import io.micronaut.core.annotation.Introspected
import tech.garymyers.wms.domain.IdDto
import java.util.UUID
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Introspected
data class InventoryDto(
   var id: UUID? = null,

   @field:NotBlank
   @field:Size(min = 3, max = 150)
   var manufacturer: String? = null,

   @field:NotBlank
   @field:Size(min = 3, max = 150)
   var sku: String? = null,

   @field:NotBlank
   @field:Size(min = 3, max = 150)
   var name: String? = null,

   @field:NotNull
   var picked: Boolean? = null,

   @field:Valid
   @field:NotNull
   var warehouse: IdDto? = null
) {
   constructor(entity: Inventory) :
      this(
         id = entity.id,
         manufacturer = entity.manufacturer,
         sku = entity.sku,
         name = entity.name,
         picked = entity.picked,
         warehouse = IdDto(entity.warehouse)
      )
}
