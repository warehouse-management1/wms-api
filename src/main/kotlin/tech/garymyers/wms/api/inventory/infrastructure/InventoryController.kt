package tech.garymyers.wms.api.inventory.infrastructure

import io.micronaut.http.HttpStatus
import io.micronaut.http.HttpStatus.CREATED
import io.micronaut.http.MediaType.APPLICATION_JSON
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.annotation.Status
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule.IS_AUTHENTICATED
import io.micronaut.validation.Validated
import tech.garymyers.wms.api.error.NotFoundException
import tech.garymyers.wms.api.inventory.InventoryDto
import tech.garymyers.wms.api.inventory.InventoryService
import tech.garymyers.wms.api.inventory.InventoryValidator
import tech.garymyers.wms.api.warehouse.WarehouseService
import tech.garymyers.wms.domain.ContollerPage
import tech.garymyers.wms.domain.StandardControllerPageRequest
import tech.garymyers.wms.extensions.toControllerPage
import java.util.UUID
import javax.inject.Inject
import javax.validation.Valid

@Validated
@Secured(IS_AUTHENTICATED)
@Controller("/api/inventory")
class InventoryController @Inject constructor(
   private val inventoryService: InventoryService,
   private val inventoryValidator: InventoryValidator,
   private val warehouseService: WarehouseService,
) {

   @Get("/{id}", produces = [APPLICATION_JSON])
   fun readOne(
      @PathVariable("id") id: UUID
   ): InventoryDto? =
      inventoryService
         .findOne(id)
         ?.let { InventoryDto(it) }

   @Get("/warehouse/{warehouseId}{?pageRequest*}", produces = [APPLICATION_JSON])
   fun readAll(
      @PathVariable("warehouseId") warehouseId: UUID,
      @QueryValue("pageRequest") pageRequest: StandardControllerPageRequest?,
      authentication: Authentication,
   ): ContollerPage<InventoryDto> {
      val warehouse = warehouseService.findOne(warehouseId) ?: throw NotFoundException("Unable to find warehouse $warehouseId")
      val requested = pageRequest ?: StandardControllerPageRequest()

      return inventoryService
         .findAllForWarehouse(warehouse, requested.toPageable())
         .toControllerPage(requested) { InventoryDto(it) }
   }

   @Status(CREATED)
   @Post(processes = [APPLICATION_JSON])
   fun create(
      @Valid @Body
      dto: InventoryDto,
   ): InventoryDto {
      val inventory = inventoryValidator.validateCreate(dto)

      return InventoryDto(inventoryService.save(inventory))
   }

   @Put("/{id}", processes = [APPLICATION_JSON])
   fun update(
      @PathVariable("id") id: UUID,
      @Valid @Body
      dto: InventoryDto,
   ): InventoryDto {
      val inventory = inventoryValidator.validateUpdate(id, dto)

      return InventoryDto(inventoryService.update(inventory))
   }
}
