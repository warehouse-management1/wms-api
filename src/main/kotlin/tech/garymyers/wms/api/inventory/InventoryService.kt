package tech.garymyers.wms.api.inventory

import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import tech.garymyers.wms.api.inventory.infrastructure.InventoryRepository
import tech.garymyers.wms.api.warehouse.Warehouse
import tech.garymyers.wms.extensions.orNull
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InventoryService @Inject constructor(
   private val inventoryRepository: InventoryRepository
) {
   fun findOne(id: UUID): Inventory? =
      inventoryRepository.findById(id).orNull()

   fun findAllForWarehouse(warehouse: Warehouse, pageable: Pageable): Page<Inventory> =
      inventoryRepository.findByWarehouse(warehouse, pageable)

   fun save(inventory: Inventory): Inventory =
      inventoryRepository.save(inventory)

   fun update(inventory: Inventory): Inventory =
      inventoryRepository.update(inventory)
}
