package tech.garymyers.wms.api.inventory

import tech.garymyers.wms.api.error.NotFoundException
import tech.garymyers.wms.api.error.SystemValidationException
import tech.garymyers.wms.api.inventory.infrastructure.InventoryRepository
import tech.garymyers.wms.api.warehouse.infrastructure.WarehouseRepository
import tech.garymyers.wms.domain.ValidatorBase
import tech.garymyers.wms.extensions.orNull
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InventoryValidator @Inject constructor(
   private val inventoryRepository: InventoryRepository,
   private val warehouseRepository: WarehouseRepository,
) : ValidatorBase() {

   @Throws(NotFoundException::class, SystemValidationException::class)
   fun validateCreate(dto: InventoryDto): Inventory {
      val warehouse = warehouseRepository.findById(dto.warehouse!!.id!!) ?: throw NotFoundException("Unable to find warehouse ${dto.warehouse!!.id!!}")

      return Inventory(dto, warehouse)
   }

   @Throws(NotFoundException::class, SystemValidationException::class)
   fun validateUpdate(id: UUID, dto: InventoryDto): Inventory {
      val existingInventory = inventoryRepository.findById(id).orNull() ?: throw NotFoundException("Unable to find inventory $id")
      val warehouse = warehouseRepository.findById(dto.warehouse!!.id!!) ?: throw NotFoundException("Unable to find warehouse ${dto.warehouse!!.id!!}")

      return existingInventory.copy(manufacturer = dto.manufacturer!!, sku = dto.sku!!, name = dto.name!!, picked = dto.picked!!, warehouse = warehouse)
   }
}
