package tech.garymyers.wms.api.inventory

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Relation
import io.micronaut.data.annotation.Relation.Kind.MANY_TO_ONE
import tech.garymyers.wms.api.warehouse.Warehouse
import tech.garymyers.wms.domain.Identifiable
import java.time.OffsetDateTime
import java.util.UUID

@MappedEntity
data class Inventory(

   @field:Id
   @field:GeneratedValue
   var id: UUID? = null,

   @field:GeneratedValue
   var timeCreated: OffsetDateTime? = null,

   @field:GeneratedValue
   var timeUpdated: OffsetDateTime? = null,

   var manufacturer: String,

   var sku: String,

   var name: String,

   val picked: Boolean,

   @field:Relation(value = MANY_TO_ONE)
   var warehouse: Warehouse,

) : Identifiable {

   constructor(dto: InventoryDto, warehouse: Warehouse) :
      this(
         manufacturer = dto.manufacturer!!,
         barcode = dto.sku!!,
         name = dto.name!!,
         picked = dto.picked!!,
         warehouse = warehouse,
      )

   constructor(manufacturer: String, barcode: String, name: String, picked: Boolean, warehouse: Warehouse) :
      this(
         id = null,
         manufacturer = manufacturer,
         sku = barcode,
         name = name,
         picked = picked,
         warehouse = warehouse,
      )

   override fun myId(): UUID? = id
}
