package tech.garymyers.wms.api.warehouse

import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.api.error.NotFoundException
import tech.garymyers.wms.api.error.ValidationError
import tech.garymyers.wms.api.warehouse.infrastructure.WarehouseRepository
import tech.garymyers.wms.domain.ValidatorBase
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WarehouseValidator @Inject constructor(
   private val warehouseRepository: WarehouseRepository
) : ValidatorBase() {
   fun validateCreate(dto: WarehouseDto, company: Company): Warehouse {
      doValidation { errors ->
         if (warehouseRepository.existsByLocationAndCompany(dto.location!!, company)) {
            errors.add(ValidationError(path = "location", message = "location with name ${dto.location} already exists for your company"))
         }
      }

      return Warehouse(
         dto = dto,
         company = company
      )
   }

   fun validateUpdate(id: UUID, dto: WarehouseDto, company: Company): Warehouse {
      val warehouse = warehouseRepository.findById(id) ?: throw NotFoundException("Unable to find warehouse using $id")

      return warehouse.copy(location = dto.location!!)
   }
}
