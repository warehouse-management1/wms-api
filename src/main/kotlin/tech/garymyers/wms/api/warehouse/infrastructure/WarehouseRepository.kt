package tech.garymyers.wms.api.warehouse.infrastructure

import io.micronaut.data.annotation.Join
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.data.repository.GenericRepository
import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.api.warehouse.Warehouse
import java.util.UUID

@JdbcRepository
interface WarehouseRepository : GenericRepository<Warehouse, UUID> { // used GenericRepository to allow the mapping of Company

   @Join(value = "company")
   fun findById(id: UUID): Warehouse?

   @Join(value = "company")
   fun findAllByCompany(company: Company, pageable: Pageable): Page<Warehouse>

   @Join(value = "company")
   fun existsByLocationAndCompany(location: String, company: Company): Boolean

   fun save(warehouse: Warehouse): Warehouse

   fun update(warehouse: Warehouse): Warehouse
}
