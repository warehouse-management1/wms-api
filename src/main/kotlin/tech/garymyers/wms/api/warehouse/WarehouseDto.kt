package tech.garymyers.wms.api.warehouse

import io.micronaut.core.annotation.Introspected
import java.util.UUID
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Introspected
data class WarehouseDto(
   var id: UUID? = null,

   @field:NotBlank
   @field:Size(min = 3, max = 150)
   var location: String? = null,
) {
   constructor(entity: Warehouse) :
      this(
         id = entity.id,
         location = entity.location,
      )
}
