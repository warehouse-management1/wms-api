package tech.garymyers.wms.api.warehouse

import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.api.warehouse.infrastructure.WarehouseRepository
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WarehouseService @Inject constructor(
   private val warehouseRepository: WarehouseRepository
) {
   fun findOne(id: UUID): Warehouse? =
      warehouseRepository.findById(id)

   fun findAll(company: Company, pageable: Pageable): Page<Warehouse> =
      warehouseRepository.findAllByCompany(company, pageable)

   fun save(warehouse: Warehouse): Warehouse =
      warehouseRepository.save(warehouse)

   fun update(warehouse: Warehouse): Warehouse =
      warehouseRepository.update(warehouse)
}
