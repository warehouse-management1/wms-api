package tech.garymyers.wms.api.warehouse

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Relation
import io.micronaut.data.annotation.Relation.Kind.MANY_TO_ONE
import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.domain.Identifiable
import java.time.OffsetDateTime
import java.util.UUID

@MappedEntity
data class Warehouse(

   @field:Id @field:GeneratedValue
   var id: UUID? = null,

   @field:GeneratedValue
   var timeCreated: OffsetDateTime? = null,

   @field:GeneratedValue
   var timeUpdated: OffsetDateTime? = null,

   var location: String,

   @field:Relation(value = MANY_TO_ONE)
   var company: Company
) : Identifiable {
   constructor(location: String, company: Company) :
      this(
         id = null,
         location = location,
         company = company,
      )

   constructor(dto: WarehouseDto, id: UUID? = null, company: Company) :
      this(
         id = id,
         location = dto.location!!,
         company = company,
      )

   override fun myId(): UUID? = id
}
