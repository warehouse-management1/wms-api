package tech.garymyers.wms.api.warehouse.infrastructure

import io.micronaut.http.HttpStatus.CREATED
import io.micronaut.http.MediaType.APPLICATION_JSON
import io.micronaut.http.annotation.*
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule.IS_AUTHENTICATED
import io.micronaut.validation.Validated
import tech.garymyers.wms.api.authentication.user.UserAuthenticationService
import tech.garymyers.wms.api.error.NotFoundException
import tech.garymyers.wms.api.warehouse.WarehouseDto
import tech.garymyers.wms.api.warehouse.WarehouseService
import tech.garymyers.wms.api.warehouse.WarehouseValidator
import tech.garymyers.wms.domain.ContollerPage
import tech.garymyers.wms.domain.StandardControllerPageRequest
import tech.garymyers.wms.extensions.toControllerPage
import java.util.*
import javax.inject.Inject
import javax.validation.Valid

@Validated
@Secured(IS_AUTHENTICATED)
@Controller("/api/warehouse")
class WarehouseController @Inject constructor(
   private val userAuthenticationService: UserAuthenticationService,
   private val warehouseService: WarehouseService,
   private val warehouseValidator: WarehouseValidator,
) {

   @Get("/{id}", produces = [APPLICATION_JSON])
   fun fetchOne(
      @PathVariable("id") id: UUID
   ): WarehouseDto? =
      warehouseService.findOne(id)?.let { WarehouseDto(it) }

   @Get(value = "/{?pageRequest*}", produces = [APPLICATION_JSON])
   fun fetchAll(
      @QueryValue("pageRequest") pageRequest: StandardControllerPageRequest?,
      authentication: Authentication,
   ): ContollerPage<WarehouseDto> {
      val user = userAuthenticationService.findUser(authentication) ?: throw NotFoundException("Unable to find logged in user")
      val requested = pageRequest ?: StandardControllerPageRequest()

      return warehouseService
         .findAll(user.myCompany(), requested.toPageable())
         .toControllerPage(requested) { WarehouseDto(it) }
   }

   @Status(CREATED)
   @Post(processes = [APPLICATION_JSON])
   fun create(
      @Valid @Body
      warehouseDto: WarehouseDto,
      authentication: Authentication,
   ): WarehouseDto {
      val user = userAuthenticationService.findUser(authentication) ?: throw NotFoundException("Unable to find logged in user")
      val warehouse = warehouseValidator.validateCreate(warehouseDto, user.myCompany())

      return WarehouseDto(warehouseService.save(warehouse))
   }

   @Put("/{id}", processes = [APPLICATION_JSON])
   fun update(
      @PathVariable("id") id: UUID,
      @Valid @Body
      warehouseDto: WarehouseDto,
      authentication: Authentication,
   ): WarehouseDto {
      val user = userAuthenticationService.findUser(authentication) ?: throw NotFoundException("Unable to find logged in user")
      val warehouse = warehouseValidator.validateUpdate(id, warehouseDto, user.myCompany())

      return WarehouseDto(warehouseService.update(warehouse))
   }
}
