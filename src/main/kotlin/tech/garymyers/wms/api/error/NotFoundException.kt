package tech.garymyers.wms.api.error

import java.util.UUID

class NotFoundException(
   message: String
) : Exception(message) {
   constructor(id: UUID) :
      this(message = id.toString())
}
