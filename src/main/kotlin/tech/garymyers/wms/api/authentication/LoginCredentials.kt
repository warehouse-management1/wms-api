package tech.garymyers.wms.api.authentication

import io.micronaut.security.authentication.UsernamePasswordCredentials
import tech.garymyers.wms.api.authentication.user.AuthenticatedUser
import java.util.UUID

class LoginCredentials(
   username: String? = null,
   password: String? = null,
   var companyId: UUID? = null
) : UsernamePasswordCredentials(
   username,
   password
) {
   constructor(authenticatedUser: AuthenticatedUser) :
      this(
         username = authenticatedUser.username,
         password = authenticatedUser.password,
         companyId = authenticatedUser.id,
      )
}
