package tech.garymyers.wms.api.authentication.user

import tech.garymyers.wms.api.company.Company
import java.util.UUID

interface User {
   fun myId(): UUID
   fun myCompany(): Company
}
