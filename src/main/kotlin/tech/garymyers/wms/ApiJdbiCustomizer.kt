package tech.garymyers.wms

import io.micronaut.configuration.jdbi.JdbiCustomizer
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import javax.inject.Singleton

@Singleton
class ApiJdbiCustomizer : JdbiCustomizer {
   override fun customize(jdbi: Jdbi) {
      jdbi.installPlugin(KotlinPlugin())
   }
}
