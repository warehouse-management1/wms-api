CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE FUNCTION last_updated_column_fn()
    RETURNS TRIGGER AS
$$
BEGIN
    new.time_updated := clock_timestamp();

    IF new.id <> old.id THEN -- help ensure that the uu_row_id can't be updated once it is created
        RAISE EXCEPTION 'cannot update id once it has been created';
    END IF;
RETURN new;
END;
$$
LANGUAGE plpgsql;

CREATE TABLE company
(
    id           UUID        DEFAULT uuid_generate_v1()   PRIMARY KEY NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                NOT NULL,
    name         VARCHAR(50) CHECK ( char_length(trim(name)) > 1 )    NOT NULL
);
CREATE TRIGGER update_company_trg
    BEFORE UPDATE
    ON company
    FOR EACH ROW
    EXECUTE PROCEDURE last_updated_column_fn();

CREATE TABLE sec_user
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY    NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                 NOT NULL,
    username     VARCHAR(100) CHECK ( char_length(trim(username)) > 1) NOT NULL,
    password     TEXT CHECK ( char_length(trim(password)) > 1 )        NOT NULL,
    enabled      BOOLEAN     DEFAULT TRUE                              NOT NULL,
    company_id   UUID REFERENCES company (id)                          NOT NULL,
    UNIQUE (username, company_id, enabled)
);
CREATE TRIGGER update_sec_user_trg
    BEFORE UPDATE
    ON sec_user
    FOR EACH ROW
    EXECUTE PROCEDURE last_updated_column_fn();
CREATE INDEX sec_user__company_id_idx ON sec_user (company_id);

CREATE TABLE warehouse
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY     NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                  NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                  NOT NULL,
    location     VARCHAR(150) CHECK ( char_length(trim(location)) > 1 ) NOT NULL,
    company_id   UUID REFERENCES company (id)                           NOT NULL,
    UNIQUE (location, company_id)
);
CREATE TRIGGER update_warehouse_trg
    BEFORE UPDATE
    ON warehouse
    FOR EACH ROW
    EXECUTE PROCEDURE last_updated_column_fn();
CREATE INDEX warehouse__company_id_idx ON warehouse(company_id);

CREATE TABLE inventory
(
    id           UUID        DEFAULT uuid_generate_v1() PRIMARY KEY         NOT NULL,
    time_created TIMESTAMPTZ DEFAULT clock_timestamp()                      NOT NULL,
    time_updated TIMESTAMPTZ DEFAULT clock_timestamp()                      NOT NULL,
    manufacturer VARCHAR(150) CHECK ( char_length(trim(manufacturer)) > 1 ) NOT NULL,
    sku          VARCHAR(150) CHECK ( char_length(trim(sku)) > 1 )          NOT NULL,
    name         VARCHAR(150) CHECK ( char_length(trim(name)) > 1)          NOT NULL,
    picked       BOOLEAN     DEFAULT FALSE                                  NOT NULL,
    warehouse_id UUID REFERENCES warehouse (id)                             NOT NULL
);
CREATE TRIGGER update_inventory_trg
    BEFORE UPDATE
    ON inventory
    FOR EACH ROW
    EXECUTE PROCEDURE last_updated_column_fn();

CREATE TABLE warehouse_inventory
(
    warehouse_id UUID REFERENCES warehouse(id) NOT NULL,
    inventory_id UUID REFERENCES inventory(id) NOT NULL,
    UNIQUE (warehouse_id, inventory_id)
);
