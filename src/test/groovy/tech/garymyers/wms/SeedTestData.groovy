package tech.garymyers.wms

import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.IgnoreIf
import spock.lang.Requires
import spock.lang.Specification
import tech.garymyers.wms.api.authentication.UserTestDataLoaderService
import tech.garymyers.wms.api.authentication.user.User
import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.api.company.CompanyTestDataLoaderService
import tech.garymyers.wms.api.inventory.Inventory
import tech.garymyers.wms.api.inventory.InventoryTestDataLoaderService
import tech.garymyers.wms.api.warehouse.Warehouse
import tech.garymyers.wms.api.warehouse.WarehouseTestDataLoaderService
import tech.garymyers.wms.domain.TruncateDatabaseService

import javax.inject.Inject

@Requires({ System.getenv("SEED_DATA")?.equals("true") })
@MicronautTest(transactional = false, environments = "load")
class SeedTestData extends Specification {
   @Inject CompanyTestDataLoaderService companyTestDataLoaderService
   @Inject InventoryTestDataLoaderService inventoryTestDataLoaderService
   @Inject TruncateDatabaseService truncateDatabaseService
   @Inject UserTestDataLoaderService userTestDataLoaderService
   @Inject WarehouseTestDataLoaderService warehouseTestDataLoaderService

   void setup() {
      truncateDatabaseService.truncate()
   }

   void "load data" () {
      given:
      final List<Company> companies = companyTestDataLoaderService.load(2).toList()
      final List<User> users = companies.stream().flatMap {userTestDataLoaderService.load(2, it) }.peek { println(it) }.toList()
      final List<Warehouse> warehouses = companies.stream().flatMap { warehouseTestDataLoaderService.load(2, it) }.toList()
      final List<Inventory> inventories = warehouses.stream().flatMap { inventoryTestDataLoaderService.load(100, it) }.toList()

      expect:
      companies.size() == 2
      users.size() == 4
      warehouses.size() == 4
      inventories.size() == 400

      cleanup:
      users.each {
         println it.username + " - " + it.password
      }
   }
}
