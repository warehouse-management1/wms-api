package tech.garymyers.wms.api.company.infrastructure

import groovy.json.JsonBuilder
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.wms.api.company.CompanyDto
import tech.garymyers.wms.api.company.CompanyTestDataLoader
import tech.garymyers.wms.api.company.CompanyTestDataLoaderService
import tech.garymyers.wms.domain.ControllerSpecificationBase

import javax.inject.Inject

import static io.micronaut.http.HttpStatus.BAD_REQUEST

@MicronautTest(transactional = false)
class CompanyControllerSpecification extends ControllerSpecificationBase {
   @Inject CompanyRepository companyRepository

   void 'fetch one'() {
      given:
      final company = companyTestDataLoaderService.single()

      when:
      def result = get("/company/${company.getId()}")

      then:
      notThrown(Exception)
      with(result) {
         id == company.id.toString()
         name == company.name
      }
   }

   void 'fetch all' () {
      given:
      final companies = companyTestDataLoaderService.load(50).collect {new CompanyDto(it) }.toList()
      final firstTenCompanies = [new CompanyDto(company), *(companies[0..8])]

      when: 'Fetch default page'
      def result = get("/company")
      println new JsonBuilder( firstTenCompanies ).toPrettyString()

      then:
      notThrown(Exception)
      with(result) {
         elements.size() == 10
         elements
            .each{ it['id'] = UUID.fromString(it['id']) }
            .collect { new CompanyDto(it) }.sort{ o1, o2 -> o1.id <=> o2.id }
            == firstTenCompanies.sort{ o1, o2 -> o1.id <=> o2.id }
      }
   }

   void 'post success' () {
      given:
      final company = CompanyTestDataLoader.single()

      when:
      def result = post("/company", new CompanyDto(company))

      then:
      notThrown(Exception)
      result.id != null
      result.name == company.name
   }

   void 'post null name' () {
      when:
      post("/company", ['name': null])

      then:
      final ex = thrown(HttpClientResponseException)
      ex.status == BAD_REQUEST
   }

   void 'post blank name' () {
      when:
      post("/company", ['name': '   '])

      then:
      final ex = thrown(HttpClientResponseException)
      ex.status == BAD_REQUEST
   }

   void 'put success' () {
      given:
      final company = companyTestDataLoaderService.single()

      when:
      def result = put("/company/${company.id}", ['name': 'New name'])

      then:
      notThrown(Exception)
      with(result) {
         UUID.fromString(id) == company.id
         name == 'New name'
      }
   }

   void 'put null name' () {
      given:
      final company = companyTestDataLoaderService.single()

      when:
      put("/company/${company.id}", ['name': null])

      then:
      final ex = thrown(HttpClientResponseException)
      ex.status == BAD_REQUEST
   }
}
