package tech.garymyers.wms.api.warehouse

import com.github.javafaker.Faker
import io.micronaut.context.annotation.Requires
import tech.garymyers.wms.api.company.Company
import tech.garymyers.wms.api.warehouse.infrastructure.WarehouseRepository

import javax.inject.Singleton
import java.util.stream.IntStream
import java.util.stream.Stream

class WarehouseTestDataLoader {
   static Stream<Warehouse> load(int numberIn = 1, Company company) {
      final number = numberIn > 0 ? numberIn : 1
      final faker = new Faker()
      final gameOfThrones = faker.gameOfThrones()

      return IntStream.range(0, number).mapToObj {
         return new Warehouse(
            gameOfThrones.character(),
            company
         )
      }
   }

   static Warehouse single(Company company) {
      return load(1, company).findFirst().orElseThrow { new Exception("Unable to create Company") }
   }
}

@Singleton
@Requires(env = ["test"])
class WarehouseTestDataLoaderService {
   private final WarehouseRepository warehouseRepository

   WarehouseTestDataLoaderService(WarehouseRepository warehouseRepository) {
      this.warehouseRepository = warehouseRepository
   }

   Stream<Warehouse> load(int numberIn, Company company) {
      return WarehouseTestDataLoader.load(numberIn, company)
         .map(warehouseRepository::save) as Stream<Warehouse>
   }

   Warehouse single(Company company) {
      return load(1, company).findFirst().orElseThrow { new Exception("Unable to create Company") }
   }
}
