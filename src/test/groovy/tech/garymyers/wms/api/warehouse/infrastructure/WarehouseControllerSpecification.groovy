package tech.garymyers.wms.api.warehouse.infrastructure

import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.wms.api.warehouse.WarehouseDto
import tech.garymyers.wms.api.warehouse.WarehouseTestDataLoader
import tech.garymyers.wms.api.warehouse.WarehouseTestDataLoaderService
import tech.garymyers.wms.domain.ControllerSpecificationBase

import javax.inject.Inject

@MicronautTest(transactional = false)
class WarehouseControllerSpecification extends ControllerSpecificationBase {
   @Inject WarehouseTestDataLoaderService warehouseTestDataLoaderService

   void 'fetch one' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company)

      when:
      def result = get("/warehouse/${warehouse.id}")

      then:
      notThrown(Exception)
      with(result) {
         UUID.fromString(id) == warehouse.id
         location == warehouse.location
      }
   }

   void 'fetch all' () {
      given:
      final warehouses = warehouseTestDataLoaderService.load(20, company).map { new WarehouseDto(it) }.toList()
      final firstTenWarehouses = warehouses[0..9]

      when:
      def result = get("/warehouse")

      then:
      notThrown(Exception)
      with(result) {
         elements.size() == 10
         elements.each{ it['id'] = UUID.fromString(it['id']) }.collect { new WarehouseDto(it) } == firstTenWarehouses
      }
   }

   void 'create' () {
      given:
      final warehouse = WarehouseTestDataLoader.single(company)

      when:
      def result = post("/warehouse", warehouse)

      then:
      notThrown(Exception)
      with(result) {
         id != null
         location == warehouse.location
      }
   }

   void 'update' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company).with {new WarehouseDto(it) }

      when:
      warehouse.location = "Test update location"
      def result = put("/warehouse/${warehouse.id}", warehouse)

      then:
      notThrown(Exception)
      with(result) {
         id == warehouse.id.toString()
         location == "Test update location"
      }
   }
}
