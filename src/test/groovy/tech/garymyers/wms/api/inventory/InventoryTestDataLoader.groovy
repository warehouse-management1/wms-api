package tech.garymyers.wms.api.inventory

import com.github.javafaker.Faker
import io.micronaut.context.annotation.Requires
import tech.garymyers.wms.api.inventory.infrastructure.InventoryRepository
import tech.garymyers.wms.api.warehouse.Warehouse

import javax.inject.Singleton
import java.util.stream.IntStream
import java.util.stream.Stream

class InventoryTestDataLoader {
   static Stream<Inventory> load(int numberIn = 1, Warehouse warehouse) {
      final number = numberIn > 0 ? numberIn : 1
      final faker = new Faker()
      final company = faker.company()
      final commerce = faker.commerce()
      final lorem = faker.lorem()

      return IntStream.range(0 , number).mapToObj {
         return new Inventory(
            company.name(),
            lorem.characters(10, 50).toUpperCase(),
            commerce.productName(),
            false,
            warehouse
         )
      }
   }

   static single(Warehouse warehouse) {
      return load(warehouse).findFirst().orElseThrow { new Exception("Unable to create Inventory") }
   }
}

@Singleton
@Requires(env = ["test"])
class InventoryTestDataLoaderService {
   private final InventoryRepository inventoryRepository

   InventoryTestDataLoaderService(InventoryRepository inventoryRepository) {
      this.inventoryRepository = inventoryRepository
   }

   Stream<Inventory> load(int numberIn = 1, Warehouse warehouse) {
      return InventoryTestDataLoader.load(numberIn, warehouse)
         .map(inventoryRepository::save) as Stream<Inventory>
   }

   Inventory single(Warehouse warehouse) {
      return load(warehouse).findFirst().orElseThrow { new Exception("Unable to create Inventory") }
   }
}
