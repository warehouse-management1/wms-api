package tech.garymyers.wms.api.inventory.infrastructure

import groovy.json.JsonBuilder
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import tech.garymyers.wms.api.inventory.InventoryDto
import tech.garymyers.wms.api.inventory.InventoryTestDataLoader
import tech.garymyers.wms.api.inventory.InventoryTestDataLoaderService
import tech.garymyers.wms.api.warehouse.WarehouseTestDataLoaderService
import tech.garymyers.wms.domain.ControllerSpecificationBase

import javax.inject.Inject

@MicronautTest(transactional = false)
class InventoryControllerSpecification extends ControllerSpecificationBase {
   @Inject InventoryTestDataLoaderService inventoryTestDataLoaderService
   @Inject WarehouseTestDataLoaderService warehouseTestDataLoaderService

   void 'fetch one' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company)
      final inventory = inventoryTestDataLoaderService.single(warehouse)

      when:
      def result = get("/inventory/${inventory.id}")
      result.id = UUID.fromString(result.id)
      result.warehouse.id = UUID.fromString(result.warehouse.id)

      then:
      notThrown(Exception)
      new InventoryDto(result) == new InventoryDto(inventory)
   }

   void 'fetch all' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company)
      final inventoryList = inventoryTestDataLoaderService.load(21, warehouse)
         .sorted { o1, o2 -> o1.id <=> o2.id }
         .map {new InventoryDto(it) }.toList()

      when:
      println new JsonBuilder(inventoryList[0..9]).toPrettyString()
      def result = get("/inventory/warehouse/${warehouse.id}")

      then:
      notThrown(Exception)
      result.elements
         .each { it.id = UUID.fromString(it.id) }
         .sort { o1, o2 -> o1.id <=> o2.id }
         .each { it.warehouse.id = UUID.fromString(it.warehouse.id) }
         .collect { new InventoryDto(it) } == inventoryList[0..9]
   }

   void 'create' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company)
      final inventory = new InventoryDto(InventoryTestDataLoader.single(warehouse))

      when:
      def result = post("/inventory", inventory)

      then:
      notThrown(Exception)
      with(result) {
         id != null
         manufacturer == inventory.manufacturer
         sku == inventory.sku
         name == inventory.name
         warehouse.id == inventory.warehouse.id
      }
   }

   void 'update' () {
      given:
      final warehouse = warehouseTestDataLoaderService.single(company)
      final inventory = inventoryTestDataLoaderService.single(warehouse)
      final toUpdate = new InventoryDto(inventory)
      toUpdate.manufacturer = 'Test manufacturer'
      toUpdate.name = 'Test name'
      toUpdate.sku = '12345'

      when:
      def result = put("/inventory/${inventory.id}", toUpdate)

      then:
      notThrown(Exception)
      with(result) {
         UUID.fromString(id) == inventory.id
         manufacturer == toUpdate.manufacturer
         name == toUpdate.name
         sku == toUpdate.sku
      }
   }
}
