FROM basedb

COPY wms-inittestdb.sh /docker-entrypoint-initdb.d/initdb.sh
COPY wms-inittestdb.sql /docker-entrypoint-initdb.d/initdb.sql
