#!/usr/bin/env sh

sed -ri "s/#log_statement = 'none'/log_statement = 'all'/g" /var/lib/postgresql/data/postgresql.conf

dropdb --if-exists wmsdb
createdb wmsdb
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "wmsdb" <<-EOSQL
   CREATE USER wmsuser WITH PASSWORD 'password';
   ALTER ROLE wmsuser SUPERUSER;
EOSQL
