FROM postgres:11.11-alpine

COPY pgpass /root/.pgpass
RUN chmod 0600 /root/.pgpass

COPY psqlrc /root/.psqlrc

RUN apk add pspg