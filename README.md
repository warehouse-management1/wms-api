# wms-api

Simple WMS API

## 3RD Party Services

### wmsdb Database
```shell
cd support
docker-compose build
docker-compose build --force-rm basedb
docker-compose build --force-rm wmsdb
docker-compose up -d --no-deps wmsdb
```

#### PSQL wmsdb prompt
```shell
docker-compose run --rm wmsdbpsql
```



#### Test Database
```shell
cd support
docker-compose build --force-rm basedb
docker-compose build --force-rm wmstestdb
docker-compose up -d --no-deps wmstestdb
```

#### PSQL wmstestdb
```shell
docker-compose run --rm wmstestdbpsql
```
